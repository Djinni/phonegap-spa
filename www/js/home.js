
document.addEventListener("deviceready", onDeviceReady, false);

var db = "";
var current_init = 0;

function load_home_page() {

    var source;
    var template;
 
    $.ajax({
        url: "templates/home.html",
        cache: true,
        success: function(data) {
            source    = data;
            template  = Handlebars.compile(source);
            document.body.innerHTML = template();
            db.transaction(populateDB, errorCB, successCB);
        }
    });
}

function populateDB(tx) {
    //tx.executeSql('DROP TABLE CharacterTable');
    //tx.executeSql('DROP TABLE InitiativeTable');
    tx.executeSql('CREATE TABLE IF NOT EXISTS CharacterTable(CharacterId INTEGER PRIMARY KEY, \
        Name TEXT NOT NULL, \
        Class TEXT NOT NULL, \
        Level INT NOT NULL)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS InitiativeTable (CharacterId INT NOT NULL,\
        Initiative INT NOT NULL, FOREIGN KEY (CharacterId) REFERENCES CharacterTable(CharacterId))');
}

function queryDB(tx) {
    tx.executeSql('SELECT CharacterTable.Name, InitiativeTable.Initiative FROM InitiativeTable join CharacterTable on InitiativeTable.CharacterId=CharacterTable.CharacterId ORDER BY InitiativeTable.Initiative desc', [], querySuccess, errorCB);
}

function querySuccess(tx,result){
        var renderTodo = function (row) {
            return "<li>" + row.Name + "</li>";
        }
        var len = result.rows.length;
        var rowOutput = "";
        var todoItems = document.getElementById("todoItems");
        for (var i = 0; i < result.rows.length; i++) {
            rowOutput = document.createElement('li');
            rowOutput.innerHTML = result.rows.item(i).Name;
            todoItems.appendChild(rowOutput); 
            //rowOutput += renderTodo(result.rows.item(i));
        }
      
        
}

function next_turn() {
    var todoItems = document.getElementById("todoItems");
    todoItems.appendChild(todoItems.firstChild);
    //todoItems.removeChild(todoItems.firstChild);
    //kids.append(kids[0]);
    //alert(kids[0]);
}

function errorCB(err) {
    alert("Error processing SQL: "+err.code);
}

function successCB() {
    db.transaction(queryDB, errorCB);
}

function onDeviceReady() {
    db = window.openDatabase("Database", "1.0", "PhoneGap Demo", 200000);
    load_home_page();
    
}
